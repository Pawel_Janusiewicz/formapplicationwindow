package formapplicationwindow;

import java.util.ArrayList;
import java.util.List;

public class OrderList {
    private List<SingleApplication> listOfOrders;

    public OrderList() {
        this.listOfOrders = new ArrayList<>();
    }
    
    public void addOrder(String name, String surname, int year, Sex sex, CivilState civilState, String content){
        SingleApplication order = new SingleApplication(name, surname, 0, sex, civilState, content);
        listOfOrders.add(order);
    }
    
}