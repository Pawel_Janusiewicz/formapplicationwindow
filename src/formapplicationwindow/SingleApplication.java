package formapplicationwindow;

public class SingleApplication {

    private String name;
    private String surname;
    private int year;
    private Sex sex;
    private CivilState civilState;
    private String content;

    public SingleApplication(String name, String surname, int year, Sex sex, CivilState civilState, String content) {
        this.name = name;
        this.surname = surname;
        this.year = year;
        this.sex = sex;
        this.civilState = civilState;
        this.content = content;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public void setSex(Sex sex) {
        this.sex = sex;
    }

    public void setCivilState(CivilState civilState) {
        this.civilState = civilState;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public int getYear() {
        return year;
    }

    public Sex getSex() {
        return sex;
    }

    public CivilState getCivilState() {
        return civilState;
    }

    public String getContent() {
        return content;
    }

    @Override
    public String toString() {
        return getName() + " " + getSurname();
    }
    
}
